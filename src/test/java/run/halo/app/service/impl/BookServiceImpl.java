package run.halo.app.service.impl;

import run.halo.app.model.projection.BookCatalogProjection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookServiceImpl {

    public static void main(String[] args) {
        List<BookCatalogProjection> bookCatalogList =  new ArrayList<>();

        for (int i =0; i<100 ;i ++){
            Integer postId = i;
            String postSlug = "slug" + i;
            String postTitle = "title" + i;
            Date createTime = new Date();
            Date updateTime = new Date();
            Integer parentId = (i - 1) <= 0 ? 0 : (i - 1)  % 10;
            bookCatalogList.add(new BookCatalogProjection(postId,
                postSlug,
                postTitle,
                createTime,
                updateTime,
                parentId));
        }

        System.out.println("===========111111");
        List<BookCatalogProjection> bookCatalogProjectionList =
            bookCatalogList2Tree(bookCatalogList);

        System.out.println("=======end====");



    }
    public static List<BookCatalogProjection> bookCatalogList2Tree(List<BookCatalogProjection> bookCatalogList) {
        List<BookCatalogProjection> bookCatalogTempList = new ArrayList<>();
        bookCatalogList.stream().forEach(bookCatalog -> {
            if (bookCatalog.getParentId().equals(0) || bookCatalog.getParentId().equals(null)){
                bookCatalogTempList.add(bookCatalog);
                bookCatalogRecursion(bookCatalogList, bookCatalog);
            }
        });
        return bookCatalogTempList;
    }

    private static void bookCatalogRecursion(List<BookCatalogProjection> bookCatalogList, BookCatalogProjection bookCatalogParent) {
        bookCatalogList.stream().forEach(bookCatalog -> {
            if (bookCatalogParent.getPostId().equals(bookCatalog.getPostId())){
                return;
            }

            if (bookCatalogParent.getPostId().equals(bookCatalog.getParentId())){
                if (bookCatalogParent.getChildBookCatalog() == null || bookCatalogParent.getChildBookCatalog().size() < 0){
                    bookCatalogParent.setChildBookCatalog(new ArrayList<>());
                }
                bookCatalogRecursion(bookCatalogList, bookCatalog);
                bookCatalogParent.getChildBookCatalog().add(bookCatalog);
            }
        });
    }
}
