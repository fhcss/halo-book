package run.halo.app.controller.content.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.halo.app.model.dto.BookDTO;
import run.halo.app.model.entity.Book;
import run.halo.app.model.entity.Category;
import run.halo.app.model.entity.Post;
import run.halo.app.model.entity.PostMeta;
import run.halo.app.model.entity.Tag;
import run.halo.app.model.enums.EncryptTypeEnum;
import run.halo.app.model.enums.PostEditorType;
import run.halo.app.model.enums.PostStatus;
import run.halo.app.model.projection.BookCatalogProjection;
import run.halo.app.service.AuthenticationService;
import run.halo.app.service.BookService;
import run.halo.app.service.CategoryService;
import run.halo.app.service.PostCategoryService;
import run.halo.app.service.PostMetaService;
import run.halo.app.service.PostService;
import run.halo.app.service.PostTagService;
import run.halo.app.service.TagService;
import run.halo.app.service.ThemeService;
import run.halo.app.utils.MarkdownUtils;
import java.util.List;
import java.util.stream.Collectors;

import static run.halo.app.model.support.HaloConst.POST_PASSWORD_TEMPLATE;
import static run.halo.app.model.support.HaloConst.SUFFIX_FTL;

/**
 * @author fancy
 */
@Component
public class BookModel {


    private final PostService postService;

    private final ThemeService themeService;

    private final BookService bookService;

    private final AuthenticationService authenticationService;

    private final PostCategoryService postCategoryService;

    private final PostTagService postTagService;

    private final PostMetaService postMetaService;

    private final CategoryService categoryService;

    private final TagService tagService;

    public BookModel (ThemeService themeService,
        BookService bookService,
        PostService postService,
        AuthenticationService authenticationService,
        PostCategoryService postCategoryService,
        PostTagService postTagService,
        PostMetaService postMetaService,
        TagService tagService,
        CategoryService categoryService
        ){
        this.themeService = themeService;
        this.bookService = bookService;
        this.postService =  postService;
        this.authenticationService = authenticationService;
        this.postCategoryService = postCategoryService;
        this.postTagService = postTagService;
        this.postMetaService = postMetaService;
        this.categoryService = categoryService;
        this.tagService = tagService;
    }

    public String listPost(Model model, String slug) {
        final Book book = bookService.getBySlug(slug);
        BookDTO bookDTO = bookService.convertTo(book);
        bookDTO.setBookCatalogTree(bookService.getBookCatalogsById(book.getId()));
        String postSlug = "";
        if (bookDTO.getBookCatalogTree() !=null && bookDTO.getBookCatalogTree().size() > 0){
            postSlug = bookDTO.getBookCatalogTree().get(0).getPostSlug();
        }

        Post post = postService.getBySlug(postSlug);

        if (post.getStatus().equals(PostStatus.INTIMATE)
            && !authenticationService.postAuthentication(post, null)) {
            model.addAttribute("slug", post.getSlug());
            model.addAttribute("type", EncryptTypeEnum.POST.getName());
            if (themeService.templateExists(POST_PASSWORD_TEMPLATE + SUFFIX_FTL)) {
                return themeService.render(POST_PASSWORD_TEMPLATE);
            }
            return "common/template/" + POST_PASSWORD_TEMPLATE;
        }

        post = postService.getById(post.getId());

        if (post.getEditorType().equals(PostEditorType.MARKDOWN)) {
            post.setFormatContent(MarkdownUtils.renderHtml(post.getOriginalContent()));
        } else {
            post.setFormatContent(post.getOriginalContent());
        }

        postService.publishVisitEvent(post.getId());

        postService.getPrevPost(post).ifPresent(
            prevPost -> model.addAttribute("prevPost", postService.convertToDetailVo(prevPost)));
        postService.getNextPost(post).ifPresent(
            nextPost -> model.addAttribute("nextPost", postService.convertToDetailVo(nextPost)));

        List<Category> categories = postCategoryService.listCategoriesBy(post.getId(), false);
        List<Tag> tags = postTagService.listTagsBy(post.getId());
        List<PostMeta> metas = postMetaService.listBy(post.getId());

        // Generate meta keywords.
        if (StringUtils.isNotEmpty(post.getMetaKeywords())) {
            model.addAttribute("meta_keywords", post.getMetaKeywords());
        } else {
            model.addAttribute("meta_keywords",
                tags.stream().map(Tag::getName).collect(Collectors.joining(",")));
        }

        // Generate meta description.
        if (StringUtils.isNotEmpty(post.getMetaDescription())) {
            model.addAttribute("meta_description", post.getMetaDescription());
        } else {
            model.addAttribute("meta_description",
                postService.generateDescription(post.getFormatContent()));
        }

        model.addAttribute("is_post", true);
        model.addAttribute("is_book", true);
        model.addAttribute("post", postService.convertToDetailVo(post));
        model.addAttribute("categories", categoryService.convertTo(categories));
        model.addAttribute("tags", tagService.convertTo(tags));
        model.addAttribute("metas", postMetaService.convertToMap(metas));
        model.addAttribute("book", bookDTO);
        return themeService.render("book");
    }
}
