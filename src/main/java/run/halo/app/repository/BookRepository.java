package run.halo.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import run.halo.app.model.entity.Book;
import run.halo.app.model.entity.Tag;
import run.halo.app.model.projection.BookCatalogProjection;
import run.halo.app.repository.base.BaseRepository;
import java.util.List;
import java.util.Optional;

public interface BookRepository extends BaseRepository<Book, Integer> {

    Optional<Book> getBySlug(String slug);

    @Query("select new  run.halo.app.model.projection.BookCatalogProjection(p.id, " +
        "p.slug, p.title, p.createTime, bp.updateTime, bp.parentId) from BookPost bp, Post p " +
        "where bp.bookId = ?1 AND bp.postId = p.id ORDER BY bp.id ASC ")
    @NonNull
    List<BookCatalogProjection> getAllBookCatalogByBookId(Integer bookId);
}
