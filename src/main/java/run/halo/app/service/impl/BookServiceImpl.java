package run.halo.app.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import run.halo.app.model.dto.BookDTO;
import run.halo.app.model.entity.Book;
import run.halo.app.model.projection.BookCatalogProjection;
import run.halo.app.repository.BookRepository;
import run.halo.app.service.BookService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl (BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
    @Override
    public Book getBySlug(String slug) {
        return bookRepository.getBySlug(slug).orElse(null);
    }

    @Override
    public BookDTO convertTo(Book book) {
        return new BookDTO().convertFrom(book);
    }

    @Override
    public  List<BookCatalogProjection> getBookCatalogsById(Integer id) {
        List<BookCatalogProjection> bookCatalogList = bookRepository.getAllBookCatalogByBookId(id);
        return bookCatalogList2Tree(bookCatalogList);
    }

    public List<BookCatalogProjection> bookCatalogList2Tree(List<BookCatalogProjection> bookCatalogList) {
        List<BookCatalogProjection> bookCatalogTempList = new ArrayList<>();
        bookCatalogList.stream().forEach(bookCatalog -> {
            if (bookCatalog.getParentId().equals(0) || bookCatalog.getParentId().equals(null)){
                bookCatalogTempList.add(bookCatalog);
                bookCatalogRecursion(bookCatalogList, bookCatalog);
            }
        });
        return bookCatalogTempList;
    }

    private void bookCatalogRecursion(List<BookCatalogProjection> bookCatalogList, BookCatalogProjection bookCatalogParent) {
        bookCatalogList.stream().forEach(bookCatalog -> {
            if (bookCatalogParent.getPostId().equals(bookCatalog.getParentId())){
               if (bookCatalogParent.getChildBookCatalog() == null || bookCatalogParent.getChildBookCatalog().size() < 0){
                   bookCatalogParent.setChildBookCatalog(new ArrayList<>());
               }
               bookCatalogRecursion(bookCatalogList, bookCatalog);
               bookCatalogParent.getChildBookCatalog().add(bookCatalog);
            }
        });
    }

}
