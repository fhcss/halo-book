package run.halo.app.service;

import org.springframework.stereotype.Service;
import run.halo.app.model.dto.BookDTO;
import run.halo.app.model.entity.Book;
import run.halo.app.model.projection.BookCatalogProjection;
import java.util.List;


public interface BookService {
    Book getBySlug(String slug);

    BookDTO convertTo(Book book);

    List<BookCatalogProjection> getBookCatalogsById(Integer id);
}
