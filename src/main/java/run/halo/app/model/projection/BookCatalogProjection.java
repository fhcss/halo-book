package run.halo.app.model.projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookCatalogProjection {

    private Integer postId;

    private String postSlug;

    private String postTitle;

    private Date createTime;

    private Date updateTime;

    private Integer parentId;

    private List<BookCatalogProjection> childBookCatalog ;

    public BookCatalogProjection (Integer postId,
        String postSlug,
        String postTitle,
        Date createTime,
        Date updateTime,
        Integer parentId) {
        this.postId = postId;
        this.postSlug = postSlug;
        this.postTitle = postTitle;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.parentId = parentId;

    }

}
