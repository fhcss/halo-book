package run.halo.app.model.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Getter
@Setter
@ToString(callSuper = true)
@RequiredArgsConstructor
@Entity
@Table(name = "book_posts", indexes = {
    @Index(name = "book_posts_post_id", columnList = "post_id"),
    @Index(name = "book_posts_book_id", columnList = "book_id")})
public class BookPost extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "custom-id")
    @GenericGenerator(name = "custom-id",
        strategy = "run.halo.app.model.entity.support.CustomIdGenerator")
    private Integer id;

    /**
     * book id.
     */
    @Column(name = "book_id")
    private Integer bookId;

    /**
     * Post id.
     */
    @Column(name = "post_id")
    private Integer postId;

    @Column(name = "parent_id")
    private Integer parentId;
}
