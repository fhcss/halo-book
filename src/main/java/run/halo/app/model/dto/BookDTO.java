package run.halo.app.model.dto;

import lombok.Data;
import run.halo.app.model.dto.base.OutputConverter;
import run.halo.app.model.entity.Book;
import run.halo.app.model.projection.BookCatalogProjection;
import java.util.Date;
import java.util.List;

@Data
public class BookDTO implements OutputConverter<BookDTO, Book> {

    private Integer id;

    private String title;

    private String slug;

    private String thumbnail;

    private String description;

    private Date createTime;

    List<BookCatalogProjection> bookCatalogTree;
}
